package xyz.vinesh.gym.activity

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.content.ContextCompat.checkSelfPermission
import android.view.View
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.content_setting.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.makeCall
import xyz.vinesh.gym.BuildConfig
import xyz.vinesh.gym.R
import xyz.vinesh.gym.enums.Constants
import xyz.vinesh.gym.util.Analytics
import xyz.vinesh.gym.util.PrefsUtils
import xyz.vinesh.gym.util.string

class SettingActivity : EnhancedActivity() {
    private val CALL_PERMISSIONS_REQUEST = 42

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        setSupportActionBar(toolbar)

        if (PrefsUtils.getString(Constants.verification_keys).isNotEmpty()) {
            tvProductKey.text = PrefsUtils.getString(Constants.verification_keys)
            tvProductKey.visibility = View.VISIBLE
        } else {
            tvProductKey.visibility = View.GONE
        }
        tvSettingCallSupport.setOnClickListener {
            if (checkSelfPermission(this,
                    Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE),
                        CALL_PERMISSIONS_REQUEST)
            } else
                makeCall(resources.getString(R.string.support_phone_number))
            Analytics.logEvent(Analytics.Events.CALL_SUPPORT, Analytics.Attributes(Analytics.AttributesKey.ACTION, string(R.string.support_phone_number)))
        }
        tvSettingMemberShip.setOnClickListener {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, it, resources.getString(R.string.pref_membership_title))
            startActivity(intentFor<MembershipsListingActivity>(), options.toBundle())
            Analytics.logEvent(Analytics.Events.ADD_MEMBERSHIP_PLAN, Analytics.Attributes(Analytics.AttributesKey.SCREEN, this::class.java.simpleName))
        }
        tvVersion.text = "${resources.getString(R.string.app_name)} v${BuildConfig.VERSION_NAME}"

        llAbout.setOnClickListener {
            startActivity(intentFor<ChangeLogActivity>())
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            CALL_PERMISSIONS_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    makeCall(resources.getString(R.string.support_phone_number))
                }
            }
        }
    }
}
