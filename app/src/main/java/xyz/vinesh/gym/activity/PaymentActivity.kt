package xyz.vinesh.gym.activity

import android.content.Context
import android.os.Bundle
import android.os.Vibrator
import android.view.KeyEvent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.content_payment.*
import org.jetbrains.anko.toast
import xyz.vinesh.gym.R
import xyz.vinesh.gym.database.*
import xyz.vinesh.gym.enums.Constants
import xyz.vinesh.gym.enums.PaymentMethods
import xyz.vinesh.gym.util.Analytics
import xyz.vinesh.gym.util.circularReveal
import java.util.*


class PaymentActivity : EnhancedActivity() {
    private var paymentMode = PaymentMethods.CASH
    private val vibrator: Vibrator by lazy { getSystemService(Context.VIBRATOR_SERVICE) as Vibrator }

    private val memberships by lazy {
        Database.create(this).membership().getAll()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        setSupportActionBar(toolbar)

        val trainee = intent.extras.getParcelable<Trainee>(Constants.TRAINEE_DETAILS_POJO.name)
        var curentMembership = Database.create(this).membership().get(trainee.membershipType.toLong())

        actionBarTitle.text = "${trainee.name}"

        tvPaymentMode.text = paymentMode.name
        tvPendingAmount.text = "Pending ₹ ${trainee.paymentPending}"


        if (curentMembership == null && trainee.paymentPending <= 0) {
            tvPendingAmount.visibility = View.GONE
            sMembershipType.visibility = View.VISIBLE

            sMembershipType.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, memberships.toStringArray())
            sMembershipType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    curentMembership = memberships[0]
                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, pos: Int, p3: Long) {
                    curentMembership = memberships[pos]
                    Analytics.logEvent(Analytics.Events.MEMBERSHIP_CHANGED)
                }

            }

        } else sMembershipType.setSelection(memberships.indexOf(curentMembership))

        etAmount.setOnEditorActionListener { _: TextView?, _: Int?, _: KeyEvent? ->
            pay(trainee, curentMembership)
            true
        }
    }

    private fun pay(trainee: Trainee, currentMembership: Membership?) {
        val paymentTable = Database.create(this).payment()
        if (etAmount.text.isNotBlank()) {
            val paidOn = Calendar.getInstance().time
            val paidAmount = etAmount.text.toString().toFloat()
            if (trainee.paymentPending <= 0) {
                val expiryDate = Calendar.getInstance()
                expiryDate.timeInMillis = trainee.expiresOn.time

                if (currentMembership != null) {
                    expiryDate.add(Calendar.MONTH, currentMembership.durationInMonths)
                    val expiresOn = expiryDate.time
                    trainee.expiresOn = expiresOn
                }
            }
            val payment = Payment(null, trainee.id, paidAmount, paymentMode, paidOn, trainee.expiresOn)
            paymentTable.insert(payment)

            val payments = paymentTable.getAll(trainee.id, trainee.expiresOn.time)
            val totalPaid = payments.sumByDouble { it.payed_amount.toDouble() }.toFloat()
            if (currentMembership != null) {
                trainee.paymentPending = currentMembership.price - totalPaid
                trainee.membershipType = currentMembership.id!!.toInt()
            } else {
                trainee.paymentPending -= paidAmount
            }
            Database.create(this).trainee().update(trainee)

            Analytics.logEvent(Analytics.Events.PAYMENT_MADE, Analytics.Attributes(Analytics.AttributesKey.PAYMENT_MODE, paymentMode.name))

        }
        toast("Done")
        finish()
    }

    fun cyclePaymentsMode(view: View) {
        val methods = PaymentMethods.values()
        paymentMode = methods[if (paymentMode.ordinal == methods.size - 1) 0 else paymentMode.ordinal + 1]
        tvPaymentMode.text = paymentMode.name
        view.circularReveal()
        vibrator.vibrate(50)
        Analytics.logEvent(Analytics.Events.PAYMENT_MODE_CHANGED)
    }
}
