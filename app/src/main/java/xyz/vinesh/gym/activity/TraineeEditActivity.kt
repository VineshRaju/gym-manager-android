package xyz.vinesh.gym.activity

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_add_trainee.*
import kotlinx.android.synthetic.main.content_add_trainee.*
import org.jetbrains.anko.*
import xyz.vinesh.gym.R
import xyz.vinesh.gym.database.Database
import xyz.vinesh.gym.database.Trainee
import xyz.vinesh.gym.enums.Constants
import xyz.vinesh.gym.util.Analytics
import xyz.vinesh.gym.util.checkAndGetValue
import xyz.vinesh.gym.util.getAge
import xyz.vinesh.gym.view.DatePicker
import java.text.SimpleDateFormat
import java.util.*

class TraineeEditActivity : EnhancedActivity() {

    lateinit var trainee: Trainee
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_trainee)
        setSupportActionBar(toolbar)

        trainee = intent.getParcelableExtra(Constants.TRAINEE_DETAILS_POJO.name)

        etId.setText(trainee.id)
        etName.setText(trainee.name)
        etMobile.setText(trainee.mobile)
        etDOB.setText(trainee.dob)
        etHeight.setText(trainee.height.toString())
        etWeight.setText(trainee.weight.toString())
        tvSex.text = trainee.sex

        etDOB.setOnFocusChangeListener { view, b ->
            if (b) {
                val newFragment = DatePicker.getInstance(trainee.dob) {
                    etDOB.setText(SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(it.time))
                }
                newFragment.show(supportFragmentManager, "datePicker")
                findViewById<EditText>(etDOB.nextFocusDownId).requestFocus()
            }
        }
        bDelete.visibility = View.VISIBLE
        bDelete.setOnClickListener {
            alert {
                titleResource = R.string.title_delete_trainee_warning

                yesButton {
                    Analytics.logEvent(Analytics.Events.DELETE_MEMBER)
                    Database.create(context = this@TraineeEditActivity).trainee().delete(trainee)
                    finish()
                }
                noButton {

                }
            }.show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.new_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.mSave -> {
                update()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun update() {
        try {
            val id = etId.checkAndGetValue()
            val name = etName.checkAndGetValue()
            val mobile = etMobile.checkAndGetValue()
            val dob = etDOB.checkAndGetValue()
            val age = getAge(dob)
            val height = etHeight.checkAndGetValue().toFloat()
            val weight = etWeight.checkAndGetValue().toFloat()
            val sex = tvSex.text.toString()
            try {
                trainee.id = id
                trainee.name = name
                trainee.name = name
                trainee.age = age
                trainee.dob = dob
                trainee.height = height
                trainee.weight = weight
                trainee.sex = sex
                Database.create(this).trainee().update(trainee)
                Analytics.logEvent(Analytics.Events.TRAINEE_EDIT)
                toast("Updated")
                finish()
            } catch (e: SQLiteConstraintException) {
                e.printStackTrace()
                longToast("Trainee with id '$id' or mobile '$mobile' already exist")
                Analytics.logEvent(Analytics.Events.DUPLICATE, Analytics.Attributes(Analytics.AttributesKey.SCREEN, this::class.java.simpleName))
            }
        } catch (e: UnsupportedOperationException) {
            longToast(e.message ?: resources.getString(R.string.error_default))
            Analytics.logEvent(Analytics.Events.INCOMPLETE_FORM, Analytics.Attributes(Analytics.AttributesKey.SCREEN, this::class.java.simpleName))
        }
    }

    fun toggleSex(view: View) {
        if (tvSex.text.toString() == "M") tvSex.text = "F" else tvSex.text = "M"
        Analytics.logEvent(Analytics.Events.SEX_CHANGED)
    }
}
