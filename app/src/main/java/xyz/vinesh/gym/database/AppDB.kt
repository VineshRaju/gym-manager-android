package xyz.vinesh.gym.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context


/**
 * Created by vineshraju on 18/10/17.
 */
@Database(entities = [(Trainee::class), (Payment::class), (Membership::class)], version = 2)
@TypeConverters(Converters::class)
abstract class AppDB : RoomDatabase() {
    abstract fun trainee(): TraineeDao
    abstract fun payment(): PaymentDao
    abstract fun membership(): MembershipDao
}