package xyz.vinesh.gym.adapter

import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.layout_payment_row.view.*
import xyz.vinesh.gym.R
import xyz.vinesh.gym.database.Payment

/**
 * Created by vineshraju on 31/10/17.
 */
class PaymentHistoryListAdapter(val payments: List<Payment>) : RecyclerView.Adapter<PaymentHistoryListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bind(payments[position])
    }

    override fun getItemCount() = payments.size
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.layout_payment_row, parent, false))


    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun bind(payment: Payment) {
            with(itemView) {
                tvAmount.text = "₹ ${payment.payed_amount}"
                tvPaymentMode.text = payment.paymentMethod.name
                tvDate.text = DateUtils.getRelativeTimeSpanString(context, payment.paidOn.time, true)
            }
        }
    }
}