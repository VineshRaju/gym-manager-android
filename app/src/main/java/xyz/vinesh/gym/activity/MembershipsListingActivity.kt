package xyz.vinesh.gym.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_membership_listing.*
import kotlinx.android.synthetic.main.content_membership_listing.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.yesButton
import xyz.vinesh.gym.App
import xyz.vinesh.gym.R
import xyz.vinesh.gym.adapter.MembershipAdapter
import xyz.vinesh.gym.database.Database
import xyz.vinesh.gym.database.Membership
import xyz.vinesh.gym.util.Analytics
import xyz.vinesh.gym.view.AddMembershipDialog

class MembershipsListingActivity : EnhancedActivity() {
    private val memberships = mutableListOf<Membership>()
    private val adapter: MembershipAdapter by lazy {
        memberships.addAll(Database.create(this@MembershipsListingActivity).membership().getAll())
        MembershipAdapter(memberships, {
            val dialog = AddMembershipDialog.getInstance(it)
            dialog.show(supportFragmentManager, "EDIT-DIALOG")
            Analytics.logEvent(Analytics.Events.MEMBERSHIP_PLAN_EDIT)
            dialog.setOnDismissListener {
                refresh()
            }
        }, { membership ->
            alert {
                titleResource = R.string.title_delete_membership
                messageResource = R.string.title_delete_membership_warning
                yesButton {
                    if (memberships.remove(membership)) {
                        Analytics.logEvent(Analytics.Events.MEMBERSHIP_PLAN_DELETE)
                        Database.create(App.context).membership().delete(membership)
                    }
                    adapter.notifyDataSetChanged()
                }
                cancelButton { }
            }.show()

        })
    }

    private fun refresh() {
        val tMemberships = Database.create(this@MembershipsListingActivity).membership().getAll()
        if (tMemberships.isEmpty()) {
            tvMembershipEmptyIllustration.visibility = View.VISIBLE
            rvMembershipList.visibility = View.GONE
        } else {
            tvMembershipEmptyIllustration.visibility = View.GONE
            rvMembershipList.visibility = View.VISIBLE
            memberships.clear()
            memberships.addAll(tMemberships)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onResume() {
        super.onResume()
        refresh()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_membership_listing)
        setSupportActionBar(toolbar)

        rvMembershipList.adapter = adapter
        rvMembershipList.layoutManager = LinearLayoutManager(this)

        fabAddNew.setOnClickListener {
            val dialog = AddMembershipDialog.getInstance()
            dialog.show(supportFragmentManager, "DIALOG")
            Analytics.logEvent(Analytics.Events.ADD_MEMBERSHIP_PLAN, Analytics.Attributes(Analytics.AttributesKey.SCREEN, this::class.java.simpleName))
            dialog.setOnDismissListener {
                refresh()
            }
        }

    }

}