package xyz.vinesh.gym.view

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import xyz.vinesh.gym.util.parseDate
import java.util.*


/**
 * Created by vineshraju on 14/12/17.
 */
class DatePicker() : DialogFragment(), DatePickerDialog.OnDateSetListener {

    companion object {
        private var c = Calendar.getInstance()

        fun getInstance(onDateSet: (date: Calendar) -> Unit): DatePicker {
            val instance = DatePicker()
            instance.onDateSet = onDateSet
            return instance
        }

        fun getInstance(dob: String, onDateSet: (date: Calendar) -> Unit): DatePicker {
            c = dob.parseDate()
            val instance = DatePicker()
            instance.onDateSet = onDateSet
            return instance
        }

    }

    lateinit var onDateSet: (date: Calendar) -> Unit
    override fun onDateSet(view: android.widget.DatePicker, year: Int, month: Int, day: Int) {
        val c = Calendar.getInstance()
        c.set(year, month, day)
        onDateSet(c)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        return DatePickerDialog(activity, this, year, month, day)
    }
}