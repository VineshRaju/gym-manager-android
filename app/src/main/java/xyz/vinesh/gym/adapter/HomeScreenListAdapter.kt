package xyz.vinesh.gym.adapter

import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import kotlinx.android.synthetic.main.layout_user_row.view.*
import xyz.vinesh.gym.R
import xyz.vinesh.gym.database.Trainee

/**
 * Created by vineshraju on 31/10/17.
 */
class HomeScreenListAdapter(val trainees: List<Trainee>, private val onCLick: (viewForTransition: View, trainee: Trainee) -> Unit, val isPaymentPendingList: Boolean = false) : RecyclerView.Adapter<HomeScreenListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bind(trainees[position], onCLick, isPaymentPendingList)
    }

    override fun getItemCount() = trainees.size
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.layout_user_row, parent, false))


    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val colorGenerator = ColorGenerator.MATERIAL

        fun bind(trainee: Trainee, listener: (viewForTransition: View, trainee: Trainee) -> Unit, isPaymentPendingList: Boolean) {
            with(itemView) {
                val drawable = TextDrawable.builder()
                        .buildRect(trainee.name.substring(0, if (trainee.name.length > 2) 2 else 1), colorGenerator.getColor(trainee.id))
                ivProfilePic.setImageDrawable(drawable)

                tvName.text = trainee.name
                if (isPaymentPendingList)
                    tvContextualLine.text = "Yet to pay ₹ ${trainee.paymentPending}"
                else
                    tvContextualLine.text = "Expired ${DateUtils.getRelativeTimeSpanString(context, trainee.expiresOn.time, true)}"
                setOnClickListener { listener(tvName, trainee) }
            }
        }
    }
}