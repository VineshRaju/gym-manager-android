package xyz.vinesh.gym.util

import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent

/**
 * Created by vineshraju on 14/11/17.
 */
object Analytics {
    enum class Events {
        VERIFIED,
        UNVERIFIED,
        TRAINEE_ADDED,
        INCOMPLETE_FORM,
        DUPLICATE,
        SCREEN_UNLOCKED,
        SCREEN_UNLOCKED_CANCELLED,
        PAYMENT_MADE,
        BACK_PRESSED,
        MEMBERSHIP_CHANGED,
        PAYMENT_MODE_CHANGED,
        DETAILS_SHEET_UP,
        DETAILS_SHEET_DOWN,
        TRAINEE_EDIT,
        NO_CONNECTION_TO_VERIFY,
        ADD_MEMBER,
        ADD_MEMBERSHIP_PLAN,
        NO_VERIFICATION_KEY,
        MEMBER_CLICKED,
        EXPIRED_MEMBER_CLICKED,
        NOT_PAYED_MEMBER_CLICKED,
        SEARCH_CLICKED,
        SETTINGS_CLICKED,
        MEMBERSHIP_PLAN_EDIT,
        MEMBERSHIP_PLAN_DELETE,
        CALL_SUPPORT,
        CALL_MEMBER,
        TEXT_MEMBER,
        PAY_CLICKED,
        DELETE_MEMBER,
        SEX_CHANGED,
        VERIFY
    }

    enum class AttributesKey {
        VERIFICATION_KEY,
        PAYMENT_MODE,
        SCREEN,
        ACTION,
        STATUS,
        BUTTON
    }

    data class Attributes(var key: AttributesKey, var value: String)

    fun logEvent(event: Events, vararg attributes: Attributes) {
        val customEvent = CustomEvent(event.name)
        attributes.forEach { customEvent.putCustomAttribute(it.key.name, it.value) }
        Answers.getInstance().logCustom(customEvent)
    }
}