package xyz.vinesh.gym.activity

import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import kotlinx.android.synthetic.main.activity_search.*
import org.jetbrains.anko.intentFor
import xyz.vinesh.gym.R
import xyz.vinesh.gym.adapter.SearchResultsAdapter
import xyz.vinesh.gym.database.Database
import xyz.vinesh.gym.database.Trainee
import xyz.vinesh.gym.enums.Constants
import xyz.vinesh.gym.util.Analytics

class SearchActivity : EnhancedActivity() {
    val searchResults = mutableListOf<Trainee>()
    val adapter: SearchResultsAdapter by lazy {
        searchResults.addAll(Database.create(this@SearchActivity).trainee().getAll())
        SearchResultsAdapter(searchResults, { view, trainee ->
            Analytics.logEvent(Analytics.Events.MEMBER_CLICKED, Analytics.Attributes(Analytics.AttributesKey.SCREEN, this::class.java.simpleName))
            val intent = intentFor<TraineeDetailsActivity>(Constants.TRAINEE_DETAILS_POJO.name to trainee)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, resources.getString(R.string.title_name))
            startActivity(intent, options.toBundle())
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setSupportActionBar(toolbar)
        svSearchField.isIconified = false
        supportActionBar?.title = ""

        rvSearchResultList.adapter = adapter
        rvSearchResultList.layoutManager = LinearLayoutManager(this)

        svSearchField.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                search(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                search(query)
                return true
            }
        })
    }

    private fun search(query: String) {
        searchResults.clear()
        val res = Database.create(this@SearchActivity).trainee().search("%$query%")
        if (res.isNotEmpty())
            searchResults.addAll(res)
        adapter.notifyDataSetChanged()
    }


}
