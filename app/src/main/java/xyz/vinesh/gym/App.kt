package xyz.vinesh.gym

import android.app.Application
import android.content.Context
import net.idik.lib.cipher.so.CipherClient
import org.jetbrains.anko.toast

/**
 * Created by vineshraju on 18/10/17.
 */
class App : Application() {
    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
    }
}