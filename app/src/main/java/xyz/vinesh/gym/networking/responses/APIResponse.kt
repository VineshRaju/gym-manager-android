package xyz.vinesh.gym.networking.responses

/**
 * Created by vineshraju on 12/11/17.
 */
data class APIResponse(var status: Boolean, var message: String, var name: String?)