package xyz.vinesh.gym.util

import android.content.Context
import android.util.TypedValue
import android.widget.EditText
import xyz.vinesh.gym.R
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by vineshraju on 14/11/17.
 */
fun convertDpToPixels(dp: Float, context: Context): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.resources.displayMetrics).toInt()
}

fun convertSpToPixels(sp: Float, context: Context): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.resources.displayMetrics).toInt()
}

